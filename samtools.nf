#!/usr/bin/env nextflow

include { combine_sample_files } from '../utilities/utilities.nf'

process samtools_stats {
// Runs samtools stats
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.stats') - Output Alignment Stats File

// require:
//   ALNS
//   params.samtools$samtools_stats_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_stats'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/samtools_stats"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.stats'), emit: bam_stats

  script:
  """
  samtools stats ${parstr} ${aln} > ${aln}.stats
  """
}

process samtools_flagstat {
// Runs samtools flagstat
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.stats') - Output Alignment Stats File

// require:
//   ALNS
//   params.samtools$samtools_stats_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_stats'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/samtools_stats"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.flagstat'), emit: bam_flagstats

  script:
  """
  samtools flagstat ${parstr} ${aln} > ${aln}.flagstat
  """
}


process samtools_view {
// Runs samtools view
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.bam') - Output Alignment File

// require:
//   ALNS
//   params.samtools_view_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_view'
//  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/samtools_view"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.bam'), emit: bams

  script:
  """
  aln_name=`echo ${aln}`
  bam_name=\${aln_name%.sam}.bam

  samtools view ${parstr} ${aln} > \${bam_name}
  """
}




process samtools_faidx {
// Runs samtools faidx
//
// input:
//   path fa -  Reference FASTA
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: faidx_file
//     path(fa) -  Reference FASTA
//     path("${fa}.fai") - FASTA Index File

// require:
//   FA
//   params.samtools$samtools_faidx_parameters

  tag "${fa}"
  label 'samtools_container'
  label 'samtools_faidx'
  cache 'lenient'

  input:
  path(fa)
  val(parstr)

  output:
  tuple path("${fa}"), path("${fa}.fai"), emit: faidx_file

  script:
  """
  samtools faidx ${parstr} ${fa}
  """
}


process samtools_index {
// Runs samtools index
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.bam') - Output Alignment File

// require:
//   ALNS
//   params.samtools_index_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_index'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path(aln), path('*.bai'), emit: bams_and_bais
  tuple val(pat_name), val(run), val(dataset), path('*.bai'), emit: bais

  script:
  """
  samtools index ${parstr} ${aln} -@ ${task.cpus}
  """
}


process samtools_sort {
// Runs samtools sort
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.bam') - Output Alignment File

// require:
//   ALNS
//   params.samtools_sort_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_sort'
//  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/samtools_sort"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.sorted.bam"), emit: bams

  script:
  """
  samtools sort ${aln} -o ${dataset}-${pat_name}-${run}.sorted.bam -@ ${task.cpus}
  """
}



process samtools_rmdup {
//runs samtools rmdup
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(aln) - Alignment File
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.rmdup.bam') - Removed Dups Alignment File

// require:
//   ALNS
//   params.samtools_rmdup_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_rmdup'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/samtools_rmdup"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.rmdup.bam'), emit: bams

  script:
  """
  ALN_NAME=`echo ${aln}`
  samtools rmdup ${aln} \${ALN_NAME%.bam}.rmdup.bam
  """
}


workflow sams_to_sorted_bams_w_indices {
// require:
//   ALNS
  take:
    alns
  main:
    samtools_view(
      alns,
      params.samtools$sams_to_sorted_bams_w_indices$samtools_view_parameters)
    samtools_sort(
      samtools_view.out.bams,
      params.samtools$sams_to_sorted_bams_w_indices$samtools_sort_parameters)
    samtools_index(
      samtools_sort.out.bams,
      params.samtools$sams_to_sorted_bams_w_indices$samtools_index_parameters)
  emit:
    bams = samtools_sort.out.bams
    bais = samtools_index.out.bais
    // Need to include bams_bais here
}


workflow bams_to_sorted_bams_w_indices {
// require:
//   ALNS
  take:
    bams
    samtools_index_parameters
  main:
    samtools_index(
      bams,
      samtools_index_parameters)
    combine_sample_files(
      bams,
      samtools_index.out.bais)
  emit:
    bams = bams
    bais = samtools_index.out.bais
    bams_bais = combine_sample_files.out.combined_set
}

process samtools_faidx_fetch {
// Runs samtools faidx to make a subset FASTA from BED file
//
// input:
//   path fa -  Reference FASTA
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: faidx_file
//     path(fa) -  Reference FASTA
//     path("${fa}.fai") - FASTA Index File

// require:
//   FA
//   params.samtools$samtools_faidx_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_faidx'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/samtools_faidx"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bed)
  path fa
  val(suffix)
  val(parstr)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.${suffix}.fa"), emit: fetched_fastas

  script:
  """
  touch ${dataset}-${pat_name}-${run}.${suffix}.fa
  sed 's/\t/:/' ${bed} | sed 's/\t/-/' > ${dataset}-${pat_name}-${run}.samt.bed



  if [ -s ${dataset}-${pat_name}-${run}.samt.bed ]; then
    samtools faidx -r ${dataset}-${pat_name}-${run}.samt.bed ${parstr} ${fa} > ${dataset}-${pat_name}-${run}.${suffix}.fa
  fi
  """
}

process samtools_coverage {
// Runs samtools stats
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.stats') - Output Alignment Stats File

// require:
//   ALNS
//   params.samtools$samtools_coverage_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_coverage'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/samtools_coverage"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.coverage'), emit: bam_coverage

  script:
  """
  samtools coverage ${parstr} ${aln} > ${aln}.coverage
  """
}

process samtools_faidx_fetch_somatic {
// Runs samtools faidx to make a subset FASTA from BED file
//
// input:
//   path fa -  Reference FASTA
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: faidx_file
//     path(fa) -  Reference FASTA
//     path("${fa}.fai") - FASTA Index File

// require:
//   FA
//   params.samtools$samtools_faidx_parameters

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'samtools_container'
  label 'samtools_faidx'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/samtools_faidx"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(beds)
  path fa
  val(suffix)
  val(parstr)

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*.${suffix}.fa"), emit: fetched_fastas

  script:
  """
  sed 's/\t/:/' ${bed} | sed 's/\t/-/' > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.samt.bed
  samtools faidx -r ${dataset}-${pat_name}-${norm_run}_${tumor_run}.samt.bed ${parstr} ${fa} > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.${suffix}.fa
  """
}

process samtools_faidx_fetch_somatic_folder {
// Runs samtools faidx to make a subset FASTA from BED file
//
// input:
//   path fa -  Reference FASTA
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: faidx_file
//     path(fa) -  Reference FASTA
//     path("${fa}.fai") - FASTA Index File

// require:
//   FA
//   params.samtools$samtools_faidx_parameters

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'samtools_container'
  label 'samtools_faidx'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/samtools_faidx"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(beds)
  path fa
  val(suffix)
  val(parstr)

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*.exon_fas"), emit: fetched_fastas

  script:
  """
  mkdir -p ${dataset}-${pat_name}-${norm_run}_${tumor_run}.exon_fas
  mkdir -p tmp
  for i in `ls ${beds}`; do
    echo \${i}
    sed 's/\t/:/' ${beds}/\${i} | sed 's/\t/-/' > tmp/\${i%.bed}.samt.bed
    echo "Made tmp/\${i%.bed}.samt.bed"
    if [ -s tmp/\${i%.bed}.samt.bed ]; then
      samtools faidx -r tmp/\${i%.bed}.samt.bed ${parstr} ${fa} > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.exon_fas/\${i%.bed}.exons.fa
    fi
    echo "Made ${dataset}-${pat_name}-${norm_run}_${tumor_run}.exon_fas/\${i%.bed}.exons.fa"
  done
  echo "Done!"
  """
}


process samtools_faidx_fetch_folder {
// Runs samtools faidx to make a subset FASTA from BED file
//
// input:
//   path fa -  Reference FASTA
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: faidx_file
//     path(fa) -  Reference FASTA
//     path("${fa}.fai") - FASTA Index File

// require:
//   FA
//   params.samtools$samtools_faidx_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_faidx'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/samtools_faidx"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(beds)
  path fa
  val(suffix)
  val(parstr)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.exon_fas"), emit: fetched_fastas

  script:
  """
  mkdir -p ${dataset}-${pat_name}-${run}.exon_fas
  mkdir -p tmp
  for i in `ls ${beds}`; do
    echo \${i}
    sed 's/\t/:/' ${beds}/\${i} | sed 's/\t/-/' > tmp/\${i%.bed}.samt.bed
    echo "Made tmp/\${i%.bed}.samt.bed"
    if [ -s tmp/\${i%.bed}.samt.bed ]; then
      samtools faidx -r tmp/\${i%.bed}.samt.bed ${parstr} ${fa} > ${dataset}-${pat_name}-${run}.exon_fas/\${i%.bed}.exons.fa
    fi
    echo "Made ${dataset}-${pat_name}-${run}.exon_fas/\${i%.bed}.exons.fa"
  done
  echo "Done!"
  """
}


process bam_subsetter {

    tag "${dataset}/${pat_name}"

    label 'samtools_container'
    label 'samtools_view'
  cache 'lenient'

    publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/bam_subsetter"

    input:
    tuple val(pat_name), val(dataset), val(norm_run), path(norm_bam), path(norm_bai), val(tumor_run), path(tumor_bam), path(tumor_bai), val(rna_run), path(rna_bam), path(rna_bai), path(bed)

    output:
    tuple val(pat_name), val(dataset), path("${dataset}-${pat_name}-${norm_run}*subset.bam"), path("${dataset}-${pat_name}-${norm_run}*subset.bam.bai"), path("${dataset}-${pat_name}-${tumor_run}*subset.bam"), path("${dataset}-${pat_name}-${tumor_run}*subset.bam.bai"), path("${dataset}-${pat_name}-${rna_run}*subset.bam"), path("${dataset}-${pat_name}-${rna_run}*subset.bam.bai"), emit: subsetted_bams
    tuple val(pat_name), val(dataset), path("${dataset}-${pat_name}-${norm_run}*subset.bam"), path("${dataset}-${pat_name}-${norm_run}*subset.bam.bai"), path("${dataset}-${pat_name}-${tumor_run}*subset.bam"), path("${dataset}-${pat_name}-${tumor_run}*subset.bam.bai"), path("${dataset}-${pat_name}-${rna_run}*subset.bam"), path("${dataset}-${pat_name}-${rna_run}*subset.bam.bai"), path(bed), emit: subsetted_bams_w_bed

    script:
    """
    for bam in `ls *bam | grep -v subset`; do
      samtools view \${bam} -bS -L ${bed} > \${bam%.bam}.subset.bam
      samtools index \${bam%.bam}.subset.bam
    done
    """
}

process samtools_merge {
// Runs samtools merge
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.bam') - Output Alignment File

// require:
//   ALNS
//   params.samtools_view_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_merge'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.merged.bam'), emit: merged_bams

  script:
  """
  base_bam=`ls *chr1.bam`
  base_name=\${base_bam%.chr1.bam}.merged.bam

  samtools merge -o \${base_name} ${parstr} *chr[1-25].bam
  """
}

process samtools_mpileup {
// Runs samtools mpileup
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.bam') - Output Alignment File

// require:
//   ALNS
//   params.samtools_mpileup_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_mpileup'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  path(fa)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.pileup"), emit: pileups

  script:
  """
  samtools mpileup -f ${fa} ${parstr} ${aln} > ${dataset}-${pat_name}-${run}.pileup
  """
}

process samtools_mpileup_parallel {
// Runs samtools mpileup
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   path faidx - Reference FASTA Index
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.bam') - Output Alignment File

// require:
//   ALNS
//   params.samtools_mpileup_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_mpileup_parallel_container'
  label 'samtools_mpileup'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln), path(bai)
  tuple path(fa), path(faidx)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*_pileups"), emit: pileups

  script:
  """
  grep -v '_' ${faidx} | grep -v '-' | cut -f 1 > chrom_list

  mkdir -p ${dataset}-${pat_name}-${run}_pileups

  while read line; do
    echo \${line}
    samtools mpileup -f ${fa} -r \${line} ${aln} > ${dataset}-${pat_name}-${run}_pileups/${dataset}-${pat_name}-${run}.\${line}.pileup&
  done < chrom_list
  wait
  """
}

process samtools_check_bam_sort {
// Emits the sorted format of a BAM, if any.
//
// output:
//   tuple => emit: coord_sorted
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("coord_sorted")
//   tuple => emit: queryname_sorted
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("queryname_sorted")

// require:
//   ALNS

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)

  output:
  tuple val(pat_name), val(run), val(dataset), path("coord_sorted"), optional: true, emit: coord_sorted
  tuple val(pat_name), val(run), val(dataset), path("queryname_sorted"), optional: true, emit: queryname_sorted

  script:
  """
  samtools view -H ${aln} | grep @HD | grep queryname > queryname_sorted || rm queryname_sorted
  samtools view -H ${aln} | grep @HD | grep coordinate > coord_sorted  || rm coord_sorted
  """
}

process samtools_get_mapped_read_counts {
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.uniqly_mapped_read_count') - Output Alignment Stats File

// require:
//   ALNS
//   params.samtools$samtools_stats_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_stats'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/samtools_get_mapped_read_counts"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.mapped_read_counts'), emit: mapped_read_counts

  script:
  """
  export TMPDIR=\${PWD}
  # TOTAL_MAPPED_READS originally had sort | uniq, but it appears read 1 and
  # read 2 have the same read ID, so that was messing up the count.
  TOTAL_MAPPED_READS=`samtools view -F 0x4,0x100 ${aln} | cut -f 1,3,4 | sort | uniq | wc -l`
  UNIQLY_MAPPED_READS=`samtools view -F 0x4,0x100 ${aln} | grep -v -e 'XA:Z:' -e 'SA:Z:' | cut -f 1,3,4 | sort | uniq | wc -l`
  echo "total_mapped_reads: \${TOTAL_MAPPED_READS}" > ${dataset}-${pat_name}-${run}.mapped_read_counts
  echo "uniqly_mapped_reads: \${UNIQLY_MAPPED_READS}" >> ${dataset}-${pat_name}-${run}.mapped_read_counts
  """
}

process samtools_get_targeted_cov_stats {
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(sam) - Alignment File
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.uniqly_mapped_read_count') - Output Alignment Stats File

// require:
//   ALNS
//   params.samtools$samtools_stats_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'samtools_container'
  label 'samtools_stats'
  publishDir "${params.qc_out_dir}/${dataset}/${pat_name}/${run}/samtools_get_targeted_cov_stats"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln), path(bai)
  path(bed)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.cov_stats'), path('*.exome_stats'), emit: cov_stats

  script:
  """
  samtools view -h -L hg38_exome.bed ${aln} > exome.bam
  samtools stats exome.bam > ${dataset}-${pat_name}-${run}.exome_stats
  while read line; do
    REG=`echo \${line} | sed 's/ /:/' | sed 's/ /-/'`;
    samtools coverage -r \${REG} ${aln} --ff "UNMAP,SECONDARY,QCFAIL" | tail -n 1 >> ${dataset}-${pat_name}-${run}.cov_stats;
  done < hg38_exome.bed
  """
}
